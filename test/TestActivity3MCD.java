import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;



public class TestActivity3MCD {
	
	//Global driver variables
	WebDriver driver;
	
	//Location of chromedriver file
	final String ChromeDriveLocation = "/Users/jyothisrajan/Desktop/chromedriver";
	
	//Website we want to test
	final String baseUrl = "http://www.mcdonalds.ca";
	@Before
	public void setUp() throws Exception {
		// Selenium setup

				System.setProperty("webdriver.chrome.driver", ChromeDriveLocation);

				driver = new ChromeDriver();

				// 2. go to website

				driver.get(baseUrl);
	} 

	@After
	public void tearDown() throws Exception {
		// After you run the test case, close the browser

				Thread.sleep(5000);

				driver.close();
				
	}

	@Test
	public void test() {
		
		
		//Calling Function for TC1
		TestTC1();
		
		//Calling Function for TC2
		TestTC2();
				
		//Calling Function for TC3
		TestTC3();
			
		
		
		
	}
	
	//Function for TC1
	
	void TestTC1() {
		//1.TC1 :Title of the subscription feature is “Subscribe to my Mcd’s”
		
				//Check whether the expected result is equal to actual result
				
				WebElement elementSubscribeTitle = driver.findElement(By.cssSelector("h2.click-before-outline"));
				assertEquals("Subscribe to My McD’s®", elementSubscribeTitle.getText());
				
	}
	
	//Function for TC2
	
	void TestTC2() {
		//TC2:  Email signup - happy path
				//Steps:
				//1.Go to website
			
				//2.Enters a first name
				WebElement firstNameField = driver.findElement(By.id("firstname2"));

				firstNameField.sendKeys("Jyothis");

				
				//3.Enters an email address
				WebElement emailField = driver.findElement(By.id("email2"));

				emailField.sendKeys("jyothisrajant@gmail.com");

				
				//4.Enters first 3 digits of a valid postal code (letter-number-letter)
				WebElement postalCodeField = driver.findElement(By.id("postalcode2"));

				postalCodeField.sendKeys("M1E");

				
				//5.Presses subscribe button
				WebElement subscribeButton = driver.findElement(By.id("g-recaptcha-btn-2"));

				subscribeButton.click();
	}
	
	//Function for TC3
	
	void TestTC3() {

		//TC3:  Email signup -  negative case
		
		//Steps:
	//Presses subscribe button
		
		WebElement subscribeButton = driver.findElement(By.id("g-recaptcha-btn-2"));

		subscribeButton.click();
	}

}
